# BambangShop Receiver App
Tutorial and Example for Advanced Programming 2024 - Faculty of Computer Science, Universitas Indonesia

---

## About this Project
In this repository, we have provided you a REST (REpresentational State Transfer) API project using Rocket web framework.

This project consists of four modules:
1.  `controller`: this module contains handler functions used to receive request and send responses.
    In Model-View-Controller (MVC) pattern, this is the Controller part.
2.  `model`: this module contains structs that serve as data containers.
    In MVC pattern, this is the Model part.
3.  `service`: this module contains structs with business logic methods.
    In MVC pattern, this is also the Model part.
4.  `repository`: this module contains structs that serve as databases.
    You can use methods of the struct to get list of objects, or operating an object (create, read, update, delete).

This repository provides a Rocket web framework skeleton that you can work with.

As this is an Observer Design Pattern tutorial repository, you need to implement a feature: `Notification`.
This feature will receive notifications of creation, promotion, and deletion of a product, when this receiver instance is subscribed to a certain product type.
The notification will be sent using HTTP POST request, so you need to make the receiver endpoint in this project.

## API Documentations

You can download the Postman Collection JSON here: https://ristek.link/AdvProgWeek7Postman

After you download the Postman Collection, you can try the endpoints inside "BambangShop Receiver" folder.

Postman is an installable client that you can use to test web endpoints using HTTP request.
You can also make automated functional testing scripts for REST API projects using this client.
You can install Postman via this website: https://www.postman.com/downloads/

## How to Run in Development Environment
1.  Set up environment variables first by creating `.env` file.
    Here is the example of `.env` file:
    ```bash
    ROCKET_PORT=8001
    APP_INSTANCE_ROOT_URL=http://localhost:${ROCKET_PORT}
    APP_PUBLISHER_ROOT_URL=http://localhost:8000
    APP_INSTANCE_NAME=Safira Sudrajat
    ```
    Here are the details of each environment variable:
    | variable                | type   | description                                                     |
    |-------------------------|--------|-----------------------------------------------------------------|
    | ROCKET_PORT             | string | Port number that will be listened by this receiver instance.    |
    | APP_INSTANCE_ROOT_URL   | string | URL address where this receiver instance can be accessed.       |
    | APP_PUUBLISHER_ROOT_URL | string | URL address where the publisher instance can be accessed.       |
    | APP_INSTANCE_NAME       | string | Name of this receiver instance, will be shown on notifications. |
2.  Use `cargo run` to run this app.
    (You might want to use `cargo check` if you only need to verify your work without running the app.)
3.  To simulate multiple instances of BambangShop Receiver (as the tutorial mandates you to do so),
    you can open new terminal, then edit `ROCKET_PORT` in `.env` file, then execute another `cargo run`.

    For example, if you want to run 3 (three) instances of BambangShop Receiver at port `8001`, `8002`, and `8003`, you can do these steps:
    -   Edit `ROCKET_PORT` in `.env` to `8001`, then execute `cargo run`.
    -   Open new terminal, edit `ROCKET_PORT` in `.env` to `8002`, then execute `cargo run`.
    -   Open another new terminal, edit `ROCKET_PORT` in `.env` to `8003`, then execute `cargo run`.

## Mandatory Checklists (Subscriber)
-   [ ] Clone https://gitlab.com/ichlaffterlalu/bambangshop-receiver to a new repository.
-   **STAGE 1: Implement models and repositories**
    -   [ ] Commit: `Create Notification model struct.`
    -   [ ] Commit: `Create SubscriberRequest model struct.`
    -   [ ] Commit: `Create Notification database and Notification repository struct skeleton.`
    -   [ ] Commit: `Implement add function in Notification repository.`
    -   [ ] Commit: `Implement list_all_as_string function in Notification repository.`
    -   [ ] Write answers of your learning module's "Reflection Subscriber-1" questions in this README.
-   **STAGE 3: Implement services and controllers**
    -   [ ] Commit: `Create Notification service struct skeleton.`
    -   [ ] Commit: `Implement subscribe function in Notification service.`
    -   [ ] Commit: `Implement subscribe function in Notification controller.`
    -   [ ] Commit: `Implement unsubscribe function in Notification service.`
    -   [ ] Commit: `Implement unsubscribe function in Notification controller.`
    -   [ ] Commit: `Implement receive_notification function in Notification service.`
    -   [ ] Commit: `Implement receive function in Notification controller.`
    -   [ ] Commit: `Implement list_messages function in Notification service.`
    -   [ ] Commit: `Implement list function in Notification controller.`
    -   [ ] Write answers of your learning module's "Reflection Subscriber-2" questions in this README.

## Your Reflections
This is the place for you to write reflections:

### Mandatory (Subscriber) Reflections

#### Reflection Subscriber-1
##### 1. In this tutorial, we used RwLock<> to synchronise the use of Vec of Notifications. Explain why it is necessary for this case, and explain why we do not use Mutex<> instead?

Well, here we prefer to use RwLock<> here over Mutex<> in Rust for synchronizing the Vec of Notifications due to its ability to allow multiple readers to access the shared data concurrently, while giving exclusive access to a single writer. This is beneficial when read operations are more frequent than write operations, as is the case in our particular scenario where multiple threads should read the NOTIFICATIONS database simultaneously without blocking each other, thereby improving performance and concurrency. In contrast, using Mutex<> would be bad because Mutek<> restricts concurrency by permitting only one thread to access the NOTIFICATIONS at a time, regardless of whether it's a read or write operation, which would in practice cause performance bottlenecks, especially when multiple threads have to read our notifications often. Therefore, RwLock<> is the superior choice for our specific system requirements. 

##### 2. In this tutorial, we used lazy_static external library to define Vec and DashMap as a “static” variable. Compared to Java where we can mutate the content of a static variable via a static function, why did not Rust allow us to do so?

Well, we know that Rust's static variables are immutable by default, a programming language design choice that ensures thread safety and prevents data races. However, when global mutable state is required, such as the NOTIFICATIONS in the provided code, the lazy_static can be employed. This enables us to define a static variable with a lazy initialization expression and guarantee that the initialization occurs only once, even in the presence of multiple threads, and provides a thread-safe way to access the static variable. In src/repository/notification.rs code file, NOTIFICATIONS is initialized with an empty vector wrapped in an RwLock<> using lazy_static, ensuring exactly one single instance is used for the entire runtime of this program. 

Rust's approach to the ban of mutable static variable(s) via static function(s) and the use of lazy_static for global mutable state offers several advantages compared to the Java programming language. This approach prevents accidental modification of static variables, eliminating some bugs and some data race conditions. It promotes an explicit, controlled approach to mutable global state, indicating where and how the state is being modified. Even more, it ensures thread safety by default, as the lazy_static handles synchronization during initialization. In comparison, the Java programming language allows mutable static variables by default, which may lead to potential issues if they are not properly synchronized in the code. Rust's approach promotes safety and encourages the programmers to be more careful dealing with mutable global state.

#### Reflection Subscriber-2
##### 1. Have you explored things outside of the steps in the tutorial, for example: src/lib.rs? If not, explain why you did not do so. If yes, explain things that you have learned from those other parts of code.

Well, I have explored the src/lib.rs code. Very useful code to know. It's the main library module file in this Rust project. There are a couple of import statements and definitions of data structures, functions, and types. There are five things to note here.

The first one is lazy static variables. This code uses the lazy_static macro to define two lazy static variables: REQWEST_CLIENT and APP_CONFIG. REQWEST_CLIENT is an HTTP client from the reqwest library, initialized only once when first accessed. APP_CONFIG holds the Rust application configuration, generated using the AppConfig::generate() method. Lazy static variables are beneficial for initializing expensive resources or configurations that are shared across the application and need to be initialized only once.

The second one is environment configuration. The code uses the dotenvy library to load environment variables from one .env file in the root directory. And then the Figment library is used to merge the default configuration (AppConfig::default()) with environment variables prefixed with "APP_". This approach allows the programmers to flexibly manage the environment configuration, allowing default values to be overridden by the environment variables if needed, making the application more adaptable to different deployment environments.

The third one is structs and serialization. The AppConfig struct holds the application configuration, with fields for the instance root URL, publisher root URL, and instance name. The struct is annotated with #[derive(Debug, Deserialize, Serialize, Getters)] to automatically derive implementations for debugging, deserialization, serialization, and getter methods. The #[serde(crate = "rocket::serde")] attribute specifies the serialization/deserialization library to use. The #[getset(get = "pub with_prefix")] attribute generates public getter methods for the struct fields with a prefix.

The fourth one is error handling. Here, we see custom error handling be implemented using the Result and Error types. Result<T, E> is considered a synonym of std::result::Result<T, E>, with E the error type set to Custom<Json<ErrorResponse>>. The ErrorResponse struct represents the error response, containing a status code and an error message. The compose_error_response() function is a helper function that creates a Custom error response with the provided status code and error message, streamlining the process of creating consistent error responses throughout the application.

And then the fifth one is Rocket framework integration. The lib.rs code file integrates the Rocket web framework, using its modules for configuration management (figment), dealing with HTTP status codes (http), and JSON serialization/deserialization (serde). The #[serde(crate = "rocket::serde")] attribute makes sure that Rocket's version of serde is used consistently. This integration allows the code to leverage Rocket's features and conventions, making it easier to build web apps with Rust and Rocket.

##### 2. Since you have completed the tutorial by now and have tried to test your notification system by spawning multiple instances of Receiver, explain how Observer pattern eases you to plug in more subscribers. How about spawning more than one instance of Main app, will it still be easy enough to add to the system?

Well, it should be obvious that the Observer pattern facilitates the addition of more subscribers into the a notification system by separating the subject (publisher) from the observing objects (observers/subscribers). With this pattern, adding new subscribers does not require changes to the core logic of the Main app acting as the publisher. Indeed it can, without any meaningful effort, create new Subscriber objects and register them with the subject without affecting the existing code.

For the latter part of the question, the deployment of multiple instances of the Main app introduces additional complexity in terms of inter-instance coordination and communication. However, the core design principles of the Observer pattern, such as loose coupling and separation of concerns, remain the same. These principles promote rapid integration of new subscribers without any significant risk of compromising the existing system's functionality. The Main app instances operate independently, while the process of creating and registering new subscribers remains smooth and does not necessitate modifications to the existing codebase. Thus, the Observer pattern should be assumed a good choice for this notification system, even in multi-instance scenarios, as it facilitates  easy addition of new Subscriber objects without disrupting the system's stability or performance.

##### 3. Have you tried to make your own Tests, or enhance documentation on your Postman collection? If you have tried those features, tell us whether it is useful for your work (it can be your tutorial work or your Group Project).

Not yet, but it may be useful, yes. Adding tests could be useful for finding potential bugs and/or other issues before deployment, which is useful. And enhancing documentation could also be useful but for a different reason. Enabling me and the other teammates to understand the API and functional testing better.
